$(document).ready(function () {

  /* ===========================================
  =           Fetch data for products          =
  =========================================== */
  var PAGE = 0;
  var PER_PAGE = 9;
  var PRICE_MIN = 0;
  var PRICE_MAX = 0;
  var FILTERS = {
    category: '',
    price: [],
    size: [],
    brand: []
  };

  /**
   *
   * @description Re-render DOM based on data from .json file
   * @param {Number} page
   * @param {Number} size
   * @param {Object} FILTERS
   * @param {Boolean} filtering
   * @returns {HTML}
   */
  function fetchDataProduct (page, size, FILTERS, filtering) {
    fetch('../../db.json')
      .then(response => response.json())
      .then(json => {
        let productsArray = [...json.products];

        // Filter function
        var paginatedProducts = filterProducts(productsArray, FILTERS);

        // Initial/default state
        paginatedProducts = paginatedProducts.slice(page * size, (page * size) + size);

        // Map over array of products (filtered or initial values)
        let elements = paginatedProducts.map(elem => (
          `
          <div class="bg-white products-item">
            <div class="products-inner-item text-center">
              <span id="category-related-to" class="d-none">${elem.category}</span>
              <img src="${elem.image}" alt="${elem.name}" class="products-image">
              <p class="products-name">${elem.name}</p>
              <p class="products-price">${elem.price}$</p>
            </div>
            <div class="overlay-product">
              <img src="${elem.image}" alt="${elem.name}" class="hover-product-image" /> 
                <p class="hover-product-name">${elem.name}</p>
                <p class="hover-product-sizes">
                    <span>sizes</span>
                    <span>:</span>
                    <span>s - m - l - xl</span>
                </p>
                <p class="product-color-picker-box">
                  <span class="color-picker red-color-product"></span>
                  <span class="color-picker dark-color-product"></span>
                  <span class="color-picker cyan-color-product"></span>
                  <span class="color-picker green-color-product"></span>
                </p>
                <hr>
                <div class="d-flex align-items-center justify-content-center product-icon-box">

                  <a href="#" class="sprite-share-icon">

                  </a>
                  <a href="#" class="sprite-add-icon" title="Add to Cart">

                  </a>
                  <a href="#" class="sprite-like-icon">

                  </a>
                </div>
            </div>
          </div>
          `
        ));

        // On page load hide the animation loading squares
        $('.loading').hide();

        // If user is doing some filtering
        if (filtering) {
          $('.js-add-product').html(elements);
        } else {
          $('.js-add-product').append(elements);
        }

        // Set global counter PAGE to current value of page
        PAGE = page;

        // Check if current size of products in DOM is less than initial state of size (9)
        // and show/hide button for adding more products
        if (paginatedProducts.length < size) {
          $('.btn-products').css('cssText', 'display: none!important;');
        } else {
          $('.btn-products').css('cssText', 'display: flex!important;');
        }

      })
      .catch(error => {
        let elementError = `
          <div class="alert alert-danger mx-auto w-100 text-center" role="alert">
            <p>
              <strong>Error fethching data!</strong>
            </p>
            <p>
              Please try again later...
            </p>
          </div>
        `;
        console.error(error);
        // Update the DOM
        $('.js-show-products-info').html(elementError);
      });
  }
  fetchDataProduct(0, PER_PAGE, FILTERS, false);
  /* ===  End of Fetch data for products  === */

  /* ===========================================
  =                  Filters By                =
  =========================================== */

  // Init Popovers Bootstrap
  $('[data-toggle="popover"]').popover();

  // Category filter
  $('.js-filter-category').on('click', function (e) {
    e.preventDefault();

    FILTERS.category = $(this).attr('data-filter-category');

    // Set on clicked elem active/inactive filter color
    $('.js-filter-category').removeClass('active-category-filter');
    $(this).addClass('active-category-filter');

    // Reset page counter to 0
    PAGE = 0;

    // Call function for fetching data
    fetchDataProduct(PAGE, PER_PAGE, FILTERS, true);

    // If navbar is fixed to the top of the page and when user clicks on Category filter
    // animate page to that div with products with offset of 100px, otherwise with offset of 200px
    if ($('.js-scroll-sticky').hasClass('js-header-pos-fixed')) {
      $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
    } else {
      $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 200 }, 'slow');
    }
  });

  // Price filter
  $('.js-price-min-val').on('keyup', function () {

    // Set default values
    if (parseInt($(this).val()) > 1500) {
      $(this).val(0);
      PRICE_MIN = 0;
    }

    // Get value from user input
    PRICE_MIN = parseInt($(this).val());

    // Set default value for max
    if (!PRICE_MAX) {
      PRICE_MAX = 1500;
    }

    setTimeout(function () {
      priceFromTo(PRICE_MIN, PRICE_MAX);
      // Animate page to product div
      $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
    }, 2000);
  });

  $('.js-price-max-val').on('keyup', function () {
  
    if (parseInt($(this).val()) > 1500) {
      // Set default values
      $(this).val(0);
      PRICE_MAX = 1500;
    }

    // Get value from user input
    PRICE_MAX = parseInt($(this).val());

    // Set default value for min
    if (!PRICE_MIN) {
      PRICE_MIN = 0;
    }

    setTimeout(function () {
      priceFromTo(PRICE_MIN, PRICE_MAX);
      // Animate page to product div
      $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
    }, 2000);
  });

  // Reset price filter to default
  $('.clear-input').on('click', function (e) {
    e.preventDefault();

    FILTERS.price = [];

    // Reset jRange lib
    $('.pointer.low, .pointer.low.last-active').css('cssText', 'left: -11px!important;');
    $('.pointer-label.low, .pointer-label.low.last-active').css('cssText', 'left: -11px!important;');
    $('.pointer-label.low, .pointer-label.low.last-active').text('0');
    $('.selected-bar').css('cssText', 'width: 0!important;');
    $('.pointer.high, .pointer.high.last-active').css('cssText', 'left: -11px!important;');
    $('.pointer-label.high, .pointer-label.high.last-active').css('cssText', 'left: -11px!important;');
    $('.pointer-label.high, .pointer-label.low.last-active').text('0');

    $('.price-form')[0].reset();

    fetchDataProduct(0, PER_PAGE, FILTERS, true);
  });

  function priceFromTo (minPrice = 0, maxPrice = 1500) {

    // If both values are 0, reset to default
    if (minPrice === 0 && maxPrice === 0) {
      FILTERS.price = [];
    } else {
      FILTERS.price = [minPrice, maxPrice];
    }

    // Reset page counter to 0
    PAGE = 0;

    // Call function for fetching data
    fetchDataProduct(PAGE, PER_PAGE, FILTERS, true);
  }

  // Slider(price) filter
  $('.slider-input').jRange({
    from: 0,
    to: 1500,
    step: 10,
    width: 180,
    showScale: false,
    format: '%s',
    showLabels: true,
    isRange: true,
    theme: 'theme-blue',
    ondragend: function (priceVal) {

      // On finished drag put the price range into array
      var splittedPriceRange = priceVal.split(',');
      var minRange = parseInt(splittedPriceRange[0], 10);
      var maxRange = parseInt(splittedPriceRange[1], 10);

      // If both values are 0, reset to default
      if (minRange === 0 && maxRange === 0) {
        FILTERS.price = [];
      } else {
        FILTERS.price = [minRange, maxRange];
      }

      // Reset page counter to 0
      PAGE = 0;

      // Call function for fetching data
      fetchDataProduct(PAGE, PER_PAGE, FILTERS, true);

      // Animate page to product div
      $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
    }
  });

  // Size filter
  $('.js-filter-size').on('click', function () {

    if ($(this).prev('input').prop('checked')) {
      // Set unchecked on input checkbox
      $(this).prev('input').prop('checked', false);
      FILTERS.size = FILTERS.size.filter(elem => elem !== $(this).prev('input').attr('data-filter-size'));
      // Add checked state to checkbox
      $(this).removeClass('mark-checkbox');
    } else {
      // Set checked on input checkbox
      $(this).prev('input').prop('checked', true);
      FILTERS.size.push($(this).prev('input').attr('data-filter-size'));
      // Add checked state to checkbox
      $(this).addClass('mark-checkbox');
    }

    // Reset page counter to 0
    PAGE = 0;

    fetchDataProduct(PAGE, PER_PAGE, FILTERS, true);

    // Animate page to product div
    $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
  });

  // Brand filter
  $('.js-filter-brand').on('click', function () {

    if ($(this).prev('input').prop('checked')) {

      // Set unchecked on input checkbox
      $(this).prev('input').prop('checked', false);

      FILTERS.brand = FILTERS.brand.filter(elem => elem !== $(this).prev('input').attr('data-filter-brand'));
      
      // Add checked state to checkbox
      $(this).removeClass('mark-checkbox');

    } else {

      // Set checked on input checkbox
      $(this).prev('input').prop('checked', true);

      FILTERS.brand.push($(this).prev('input').attr('data-filter-brand'));

      // Add checked state to checkbox
      $(this).addClass('mark-checkbox');

    }

    // Reset page counter to 0
    PAGE = 0;

    fetchDataProduct(PAGE, PER_PAGE, FILTERS, true);

    // Animate page to product div
    $('html, body').animate({ scrollTop: $('.js-add-product').offset().top - 100 }, 'slow');
  });
  /* ======  End of Filters By  ====== */

  /* ===========================================
  =             Load more Products             =
  =========================================== */
  $('.btn-products').on('click', function () {

    // On btn click show more products, animate loading button
    $(this).addClass('js-spinner');

    fetchDataProduct(PAGE + 1, PER_PAGE, FILTERS);

    // Remove loading button when FETCH API is done
    $(this).removeClass('js-spinner');

  });
  /* =====  End of Load more Products  ====== */


  /* ===========================================
  =              Filter Products               =
  =========================================== */
  /**
   *
   * @description Filter the array in .json file based on user interaction (filter)
   * @param {JSON} products
   * @param {Object} FILTERS
   * @returns {Array}
   */
  function filterProducts (products, FILTERS) {
    var filteredProduct = [...products];

    // If user hasn't selected 'All' from Category filter
    // show that selected items, otherwise show default state
    if (FILTERS.category !== '') {
      filteredProduct = filteredProduct.filter(elem => {
        return elem.category === FILTERS.category;
      });
    }
    if (FILTERS.size.length) {
      filteredProduct = filteredProduct.filter(elem => {
        return FILTERS.size.includes(elem.size);
      });
    }
    if (FILTERS.price.length) {
      filteredProduct = filteredProduct.filter(elem => {
        return elem.price >= FILTERS.price[0] && elem.price <= FILTERS.price[1];
      });
    }
    if (FILTERS.brand.length) {
      filteredProduct = filteredProduct.filter(elem => {
        return FILTERS.brand.includes(elem.brand);
      });
    }
    return filteredProduct;
  }
  /* =====  End of Filter Products  ====== */


  /* ===========================================
  =               Products Show                =
  =========================================== */
  $(document).on('click', '.hover-product-image', function () {

    // Show loading animation
    $('.loading').show();

    fetch('../../db.json')
      .then(response => response.json())
      .then(json => {
        let product = [...json.singleProduct];

        let productShow = product.map(elem => (
          `
          <section class="w-100 bg-white product-show-wrapper">

            <article class="d-flex flex-wrap mx-auto justify-content-center text-center single-product-box">
              <div class="col-lg-4 col-12">
                <img src="${elem.mainImage}" alt="${elem.name}" />
                <div class="single-product-image-showcase-box">
                  <div class="product-show-small-image">
                    <img src="${elem.bootImage}" alt="${elem.productBootDescription}" />
                  </div>
                  <div class="product-show-small-image">
                    <img src="${elem.pantsImage}" alt="${elem.productPantsDescription}" />
                  </div>
                  <div class="product-show-small-image">
                    <img src="${elem.jacketImage}" alt="${elem.productJacketDescription}" />
                  </div>
                </div>
              </div>
              <div class="col-lg-5 col-12 product-description">
                <h1 class="text-lg-left text-uppercase product-headline">${elem.name}</h1>
                <p class="text-lg-left product-para-desc">
                  <span>${elem.productJacketDescription} + </span>
                  <span>${elem.productPantsDescription} + </span>
                  <span>${elem.productBootDescription}</span>
                </p>
                <div class="text-lg-left product-full-desc-para">
                  <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry</span>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing <br />
                    and typesetting industry
                  </p>
                </div>

                <hr class="product-separator-line">

                <div class="d-flex flex-wrap justify-content-lg-between justify-content-around align-items-center product-size-quantity-box">
                  <span class="product-size-picker">
                    <span>Choose size</span>
                    <span>S - M - </span>
                    <span class="selected-size">L</span>
                    <span> - XL</span>
                  </span>

                  <span class="product-quantity-picker">
                    <span>Choose Quantity</span>
                    <span class="increment-quantity">+</span>
                    <span class="choose-quantity">3</span>
                    <span class="decrement-quantity">-</span>
                  </span>
                </div>

                <hr class="product-separator-line">

                <div class="d-flex flex-wrap justify-content-lg-between justify-content-around align-items-center product-size-price-order-box">
                  <span class="product-price">
                    <span>Price: </span>
                    <span>${elem.price}</span>
                  </span>

                  <span class="product-order">
                    <a href="#">
                      <img src="dist/assets/images/icons/share-icon.png" alt="share icon" />
                    </a>
                    <a href="#">
                      <img src="dist/assets/images/icons/add-to-cart-icon1.png" alt="add to cart icon" />
                    </a>
                    <a href="#">
                      <img src="dist/assets/images/icons/like-icon.png" alt="like icon" />
                    </a>
                    <button class="btn-order-now">Order Now</button>
                  </span>
                </div>

              </div>
            </article>

          </section>
          <div id="products-show" class="clearfix">
            <p class="text-center related-to-para">
              <span class="text-uppercase related-span-text">related</span>
              <span class="text-uppercase related-span-product">products</span>
            </p>
            <p class="text-center related-para-desc-text">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry
            </p>
            <div class="container js-add-product related-products"></div>
          </div>
          `
        ));

        // Set category that is related to clicked product
        FILTERS.category = $('#category-related-to').text();
        // Call function for fetching data
        fetchDataProduct(0, 4, FILTERS, true);

        // On DOM re-render, scroll to top
        $('html, body').animate({ scrollTop: 0 }, 'slow');

        // Update the DOM
        $('.js-product-show').removeClass('container').html(productShow);

        // Hide the loading animation
        $('.loading').hide();
      })
      .catch(error => {
        let elementError = `
          <div class="alert alert-danger mx-auto w-100 text-center" role="alert">
            <p>
              <strong>Error fethching data!</strong>
            </p>
            <p>
              Please try again later...
            </p>
          </div>
        `;
        console.error(error);
        $('.js-show-products-info').html(elementError);
      });

  });
  /* ========  End of Products Show  ========= */


  /* =====================================
  =      Sticky Header Functionality     =
  ===================================== */
  $(window).on('scroll', function () {

    if ($(window).scrollTop() >= 100) {
      $('.js-scroll-sticky').addClass('js-header-pos-fixed');
    } else if ($(window).scrollTop() === 0) {
      $('.js-scroll-sticky').removeClass('js-header-pos-fixed');
    }

  });
  /* ==  End of Sticky Header Functionality  == */


  /* ===========================================
  =   Hide/show animation navbar toggler btn   =
  =========================================== */
  $('.navbar-toggler').on('click', function () {

    $('.animated-icon').toggleClass('js-animated-icon-open');

  });
  /* ==  End of Hide/show animation navbar toggler btn  == */


  /* ===========================================
  =        Hide/show back to top button        =
  =========================================== */
  $('.js-back-to-top-btn').on('click', function () {

    $('html, body').animate({ scrollTop: 0 }, 'slow');

  });

  $(window).on('scroll', function () {

    if ($(window).scrollTop() >= 200) {
      $('.js-back-to-top-btn').addClass('js-show-btn-to-top');
    } else if ($(window).scrollTop() <= 100) {
      $('.js-back-to-top-btn').removeClass('js-show-btn-to-top');
    }

  });
  /* ==  End of Hide/show back to top button  == */


  /* ===========================================
  =            Navigation on Mobile            =
  =========================================== */
  $('.navbar').on('show.bs.collapse', function () {
    $(this).addClass('mobile-open');
    $('body').addClass('add-overflow-body');
  });
  $('.navbar').on('hidden.bs.collapse', function () {
    $(this).removeClass('mobile-open');
    $('body').removeClass('add-overflow-body');
  });
  /* =====  End of Navigation on Mobile  ===== */


  /* ===========================================
  =            Animate add to Cart             =
  =========================================== */
  $(document).on('click', '.sprite-add-icon', function (e) {
    e.preventDefault();

    // Show success message div when user has added new item to cart
    $('.success-message').css('display', 'block');
    setTimeout(function () {
      $('.success-message').fadeOut();
    }, 3000);

    // Animate the number of products in Cart
    var currentItemsInCart = parseInt($('.shop-cart-number span').text());
    $('.shop-cart-number span').text(currentItemsInCart + 1).animate({ top: 6 }, 800, function () {
      $(this).animate({ top: 3 }, 800);
    });
  });
  /* =====  End of Animate add to Cart ===== */

});